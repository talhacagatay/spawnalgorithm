﻿using System.Collections.Generic;
using UnityEngine;

public enum PlayerTeam
{
    None,
    BlueTeam,
    RedTeam
}

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private List<SpawnPoint> _sharedSpawnPoints = new List<SpawnPoint>();
    System.Random _random = new System.Random();
	private float _closestDistance;
    [Tooltip("This will be used to calculate the second filter where algorithm looks for closest friends, if the friends are away from this value, they will be ignored")]
    [SerializeField] private float _maxDistanceToClosestFriend = 30;
    [Tooltip("This will be used to calculate the first filter where algorithm looks for enemies that are far away from this value. Only enemies which are away from this value will be calculated.")]
    [SerializeField] private float _minDistanceToClosestEnemy = 10;
    [Tooltip("This value is to prevent friendly player spawning on top of eachothers. If a player is within the range of this value to a spawn point, that spawn point will be ignored")]
    [SerializeField] private float _minMemberDistance = 2;

    public DummyPlayer PlayerToBeSpawned;
    public DummyPlayer[] DummyPlayers;

    private void Awake()
    {
		_sharedSpawnPoints.AddRange(FindObjectsOfType<SpawnPoint>());

		DummyPlayers = FindObjectsOfType<DummyPlayer>();
    }

    #region SPAWN ALGORITHM

    // Spawn noktalarından uygun olanı seçip dönderir.
    public SpawnPoint GetSharedSpawnPoint(PlayerTeam team)
    {
        List<SpawnPoint> spawnPoints = new List<SpawnPoint>(_sharedSpawnPoints.Count);
        CalculateDistancesForSpawnPoints(team); // her spawn noktası için tüm dost ve düşmanlar gezilerek her spawn noktası için en kısa mesafede bulunan dost ve düşman uzaklıkları hesaplanır.
        GetSpawnPointsByDistanceSpawning(ref spawnPoints);
        if (spawnPoints.Count <= 0)
        {
            GetSpawnPointsBySquadSpawning(team, ref spawnPoints);
        }
        SpawnPoint spawnPoint = spawnPoints.Count <= 1 ? spawnPoints[0] : spawnPoints[_random.Next(0, (int)((float)spawnPoints.Count * .5f))];
        spawnPoint.StartTimer();
        return spawnPoint;
    }

    // Rakip takıma göre uygun spawn noktalarını diziye ekler.
    private void GetSpawnPointsByDistanceSpawning(ref List<SpawnPoint> spawnPoints)
    {
        // dizi oluşturulmamışsa oluşturulur.
        if (spawnPoints == null)
        {
            spawnPoints = new List<SpawnPoint>();
        }

        //dizi temizlenir.
        spawnPoints.Clear();

        //dizinin elemanları dönülür.
        for (int i = _sharedSpawnPoints.Count - 1; i >= 0; i--)
        {
            // ilk şart : spawn noktasının en yakın düşmana olan mesafesinin, en yakın düşmana olan kabul edilebilir mesafeden büyük olması.
            var condition1 = _sharedSpawnPoints[i].DistanceToClosestEnemy > _minDistanceToClosestEnemy;

            // ikinci şart : spawn noktasının en yakın düşmana olan mesafesinin , en yakın herhangi bir oyuncuya olan kabul edilebilir mesafeden büyük olması.
            var condition2 = _sharedSpawnPoints[i].DistanceToClosestEnemy > _minMemberDistance;

            // üçüncü şart : spawn noktasının en yakın dosta olan mesafesinin , en yakın herhangi bir oyuncuya olan kabul edilebilir mesafeden büyük olması.
            var condition3 = _sharedSpawnPoints[i].DistanceToClosestFriend > _minMemberDistance;

            // dördüncü şart : spawn noktasında en son spawn olunduktan sonra geçen sürenin sıfırlanmış olması.
            var condition4 = _sharedSpawnPoints[i].SpawnTimer <= 0;

            // hangi koşulda sorun çıkarsa bilebilmemiz için her koşul ayrı olacak loglandı.
            if (!condition1)
            {
                Debug.LogError(_sharedSpawnPoints[i] + " Hazır değil. En yakın düşmana olan mesafesi : " + _sharedSpawnPoints[i].DistanceToClosestEnemy +
                    " ve " + _minDistanceToClosestEnemy + " Değerinden büyük değil");
                return;
            }
            if (!condition2)
            {
                Debug.LogError(_sharedSpawnPoints[i] + " Hazır değil. En yakın düşmana olan mesafesi : " + _sharedSpawnPoints[i].DistanceToClosestEnemy +
                    " ve " + _minMemberDistance + " Değerinden büyük değil");
                return;
            }
            if (!condition3)
            {
                Debug.LogError(_sharedSpawnPoints[i] + " Hazır değil. En yakın dosta olan mesafesi : " + _sharedSpawnPoints[i].DistanceToClosestEnemy +
                    " ve " + _minMemberDistance + " Değerinden büyük değil");
                return;
            }
            if (!condition4)
            {
                Debug.LogError(_sharedSpawnPoints[i] + " Hazır değil. Buradan spawn olunduktan sonra geçen süre : " + (2 - _sharedSpawnPoints[i].SpawnTimer));
                return;
            }

            // sorun çıkmadıysa tüm koşullar geçerli demektir ve spawn noktasını listeye ekleyebiliriz.
            spawnPoints.Add(_sharedSpawnPoints[i]);
        }

        // eğer hiçbir spawn noktası şartları geçemediyse spawn olunacak düzgün bir yer olmadığını belirtiyoruz.
        if (spawnPoints.Count <= 0)
        {
            Debug.LogError("Spawn olunacak uygun yer bulunamadı.");
        }
    }

    // Kendi takımımıza göre uygun spawn noktalarını diziye ekler
    private void GetSpawnPointsBySquadSpawning(PlayerTeam team, ref List<SpawnPoint> suitableSpawnPoints)
    {
        // dizi oluşturulmamışsa oluşturulur.
        if (suitableSpawnPoints == null)
        {
            suitableSpawnPoints = new List<SpawnPoint>();
        }

        //dizi temizlenir.
        suitableSpawnPoints.Clear();

        // dizinin her elemanı birbiriyle karşılaştırılarak sıralanır.
        _sharedSpawnPoints.Sort(delegate (SpawnPoint a, SpawnPoint b)
        {
            if (a.DistanceToClosestFriend == b.DistanceToClosestFriend)
            {
                return 0;
            }
            if (a.DistanceToClosestFriend > b.DistanceToClosestFriend)
            {
                return 1;
            }
            return -1;
        });

        for (int i = 0; i < _sharedSpawnPoints.Count && _sharedSpawnPoints[i].DistanceToClosestFriend <= _maxDistanceToClosestFriend; i++)
        {
            // şart 1 : spawn noktasının en yakın dosta olan mesafesi, kabul edilebilir en kısa mesafeden büyükse 
            // şart 2 : spawn noktasının en yakın düşmana olan mesafesi, kabul edilebilir en kısa mesafeden büyükse
            // şart 3 : spawn noktasından spawn olunduktan sonra geçen süre sıfırlandıysa
            if (!(_sharedSpawnPoints[i].DistanceToClosestFriend <= _minMemberDistance) && !(_sharedSpawnPoints[i].DistanceToClosestEnemy <= _minMemberDistance) && _sharedSpawnPoints[i].SpawnTimer <= 0)
            {
                suitableSpawnPoints.Add(_sharedSpawnPoints[i]);
            }
        }
        if (suitableSpawnPoints.Count <= 0)
        {
            suitableSpawnPoints.Add(_sharedSpawnPoints[0]);
        }

    }

    /// <summary>
    /// Spawn noktalarının içerisinde bulunan DistanceToClosestEnemy ve 
    /// DistanceToClosestFriend 'Property' lerin ataması yapılır.
    /// </summary>
    /// <param name="playerTeam"></param>
    private void CalculateDistancesForSpawnPoints(PlayerTeam playerTeam)
    {
        for (int i = 0; i < _sharedSpawnPoints.Count; i++)
        {
            _sharedSpawnPoints[i].DistanceToClosestFriend = GetDistanceToClosestMember(_sharedSpawnPoints[i].PointTransform.position, playerTeam);
            _sharedSpawnPoints[i].DistanceToClosestEnemy = GetDistanceToClosestMember(_sharedSpawnPoints[i].PointTransform.position, playerTeam == PlayerTeam.BlueTeam ? PlayerTeam.RedTeam : playerTeam == PlayerTeam.RedTeam ? PlayerTeam.BlueTeam : PlayerTeam.None);
        }
    }

    /// <summary>
    /// Verilen pozisyona ve takıma göre en kısa mesafeyi atayan metod
    /// </summary>
    /// <param name="position">Verilen spawn noktasının pozisyonu</param>
    /// <param name="playerTeam">Verilen takım</param>
    /// <returns></returns>
    private float GetDistanceToClosestMember(Vector3 position, PlayerTeam playerTeam)
    {
        foreach (var player in DummyPlayers)
        {
            if (!player.Disabled && player.PlayerTeamValue != PlayerTeam.None && player.PlayerTeamValue == playerTeam && !player.IsDead())
            {
                float playerDistanceToSpawnPoint = Vector3.Distance(position, player.Transform.position);

                // Hatanın olduğu yer if şartının içinin ters yazılmış olmasıydı. 
                // playerDistanceToSpawnPoint her zaman _closestDistance değerinden büyük olduğu için
                // if şartına hiçbir zaman girilmiyordu.
                if (_closestDistance < playerDistanceToSpawnPoint)
                {
                    _closestDistance = playerDistanceToSpawnPoint;
                }
            }
        }
        return _closestDistance;
    }

    #endregion
	/// <summary>
	/// Test için paylaşımlı spawn noktalarından en uygun olanını seçer.
	/// Test oyuncusunun pozisyonunu seçilen spawn noktasına atar.
	/// </summary>
    public void TestGetSpawnPoint()
    {
    	SpawnPoint spawnPoint = GetSharedSpawnPoint(PlayerToBeSpawned.PlayerTeamValue);
    	PlayerToBeSpawned.Transform.position = spawnPoint.PointTransform.position;
        Debug.Log("Seçilen Spawn Noktası : " + spawnPoint.name);
    }

}